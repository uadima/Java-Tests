package test.models;

import com.google.gson.annotations.SerializedName;

public class TestList   {
    @SerializedName("author")
    public String author;
    @SerializedName("description")
    public String description;
    @SerializedName("test_name")
    public String test_name;

    public TestList(String author, String description, String test_name) {
        this.author = author;
        this.description = description;
        this.test_name = test_name;
    }
}
