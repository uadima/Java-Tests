package test.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="question")
public class Question {
    @Element
    String question;
    @Element
    String answer1;
    @Element
    String answer2;
    @Element
    String answer3;
    @Element
    String answer4;
    @Element
    int[] rightanswer;
    boolean completed;


    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }
    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }
    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }


    public String getAnswer3() {
        return answer3;
    }
    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }
    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int[] getRightanswer() {
        return rightanswer;
    }
    public void setRightanswer(int[] rightanswer) {
        this.rightanswer = rightanswer;
    }
}
