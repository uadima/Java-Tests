package test.models;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;

import static test.Start.mainStage;
import static test.Start.stageController;

public class RefreshView {
    @FXML
    private VBox vBox;

    @FXML
    private Text textError;

    @FXML
    private JFXSpinner spinner;

    @FXML
    private JFXButton buttonRefresh;

    public RefreshView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/test/resources/fragments/refresh.fxml"));
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        bindWith();
        buttonRefresh.setOnAction(event -> {
            stageController.next(getClass().getResource("/test/resources/view/lvlChoose.fxml"));
        });
    }

    private  void bindWith(){
        textError.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.8f));
    }

    public VBox getBox()
    {
        return vBox;
    }
}
