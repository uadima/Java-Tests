package test.models;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import test.controllers.TestController;

import java.io.IOException;

public class CustomGrid {
    @FXML
    private Button button_grid;
    private int n;
    TestController context;


    public CustomGrid(TestController context, int n) {
        this.context = context;
        this.n = n;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/test/resources/fragments/button_grid.fxml"));
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Can`t load button_grid resource");
        }
        button_grid.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                context.next(n);
                System.out.println("clicked on " + n);
                context.prepareQuestion();
            }
        });
        button_grid.setOnMouseEntered(event -> {

        });
        button_grid.setOnMouseExited(event -> {

        });
        button_grid.backgroundProperty().bind(Bindings.createObjectBinding(() -> {
            BackgroundFill fill = new BackgroundFill(context.colorChoose(n), CornerRadii.EMPTY, Insets.EMPTY);
            return new Background(fill);
        }));
/*        final ObservableBooleanValue booleanCondition = (Boolean) Start.test.getQuestions().get(n).isCompleted();
        final ObservableObjectValue<Paint> paintProperty = Bindings.when(booleanCondition).then(Color.RED).otherwise(Color.DODGERBLUE);*/
    }

        public void setText (String t){
            button_grid.setText(t);
        }
        public Button getButton_grid () {
            return button_grid;
        }
    }
