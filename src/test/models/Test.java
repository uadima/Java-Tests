package test.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


@Root(name = "test")
public class Test {
    @Element
    String test_name;
    @Element
    String author;
    @Element
    String description;
    List<Question> questions = null;
    public static boolean published;
    public static double test_result;


    public Test(){
        this.test_result=0;
        this.published=false;
    }

    @ElementList(name = "questions", inline = false)
    public List<Question> getQuestions() {
        return questions;
    }
    @ElementList(name = "questions", inline= false)
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }


    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTest_name() {
        return test_name;
    }
    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }
}
