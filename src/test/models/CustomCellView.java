package test.models;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import test.interfaces.Callback;
import test.myCustomUtils.WorkIndicatorDialog;

import java.io.File;
import java.io.IOException;

import static test.Start.*;

public class CustomCellView {
    @FXML
    private VBox vBox;
    @FXML
    private Text test_name;
    @FXML
    private Text author;
    @FXML
    private Text description;
    @FXML
    private Button button;


    public CustomCellView()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/test/resources/fragments/cell.fxml"));
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        bindWith();
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Clicked on start test "+test_name.getText());
                WorkIndicatorDialog workIndicatorDialog=new WorkIndicatorDialog(mainStage,"Downloading test: "+test_name.getText());
                new Thread(new Runnable() {
                    Callback callback = workIndicatorDialog;
                    @Override
                    public void run() {
                        test = networkManager.getTest(test_name.getText());
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                callback.call();
                                File f = new File("./test.xml");
                                if(f.exists()){
                                System.out.println("Successfully downloaded test "+test_name.getText()+" Saved to "+f.getAbsolutePath());
                                    if(f.delete()){
                                        System.out.println("Deleted");
                                    }else {
                                        System.out.println("Error during delete");
                                    }
                                stageController.next(getClass().getResource("/test/resources/view/test.fxml"));}
                            }
                        });
                    }
                }).start();
                workIndicatorDialog.showDialog();
            }
        });
    }

    public void setInfo(String name, String author, String description)
    {
       this.test_name.setText(name);
       this.author.setText("@"+author);
       this.description.setText(this.description.getText()+description);
    }

    private  void bindWith(){
        test_name.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
        author.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
        description.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.82f));
    }

    public VBox getBox()
    {
        return vBox;
    }
}
