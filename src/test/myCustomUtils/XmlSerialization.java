package test.myCustomUtils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import test.models.Test;

import java.io.*;

public class XmlSerialization {
    public static boolean result;


    public static void convertObjectToXml(Test test, String filePath) {
        Serializer serializer = new Persister();
        File file =new File(filePath+test.getTest_name()+".xml");
        try
        {  serializer.write(test, file);
            String xml = file.toString();
            System.out.println(xml);
            result = true;
        }
        catch (Exception e)
        {
            System.out.println("Cannot serealize class "+e);
            result = false;
        }
    }

    public static Test fromXmlToObject(String filePath) {
        Test test = null;
        File input =new File(filePath);
        Persister serializer = new Persister();
        try
        {
            test = serializer.read(Test.class, input, false);
/*            System.out.println("Test "+test.getTest_name() +" First question is "+test.getQuestions().get(0).getQuestion());*/
            result = true;
            return test;
        }
        catch (Exception e)
        {
            System.out.println("Test deserialization error "+ e);
            result = false;
        }
        return test;
    }

}
