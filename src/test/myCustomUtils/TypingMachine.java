package test.myCustomUtils;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class TypingMachine {
    private Text story;
    String content;
    int duration=1000;

    final Animation animation = new Transition() {
        {
            setCycleDuration(Duration.millis(duration));
        }

        protected void interpolate(double frac) {
            final int length = content.length();
            final int n = Math.round(length * (float) frac);
            story.setText(content.substring(0, n));
        }

    };

    public void setStory(String content) {
        this.content = content;
    }

    public TypingMachine(Text story, int duration) {
        this.story = story;
        this.duration = duration;
        this.content=story.getText();
    }
    public TypingMachine(Text story) {
        this(story,5000);
    }

    public Animation getAnimation() {
        story.setVisible(true);
        return animation;
    } //animation.play();
}
