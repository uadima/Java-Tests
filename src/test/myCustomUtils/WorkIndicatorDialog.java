package test.myCustomUtils;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import test.interfaces.Callback;


public class WorkIndicatorDialog implements Callback {


    private final ProgressIndicator progressIndicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
    private final Stage dialog = new Stage(StageStyle.UNDECORATED);
    private final Label label = new Label();
    private final Group root = new Group();
    private Color color =  Color.web("#293945");
    private final Scene scene = new Scene(root, 330, 120, color);
    private final BorderPane mainPane = new BorderPane();
    private final VBox vbox = new VBox();



    public WorkIndicatorDialog(Window owner, String label) {
        this.label.setId("workIndicatorLabel");
        vbox.setId("WorkIndicator");
        this.scene.getStylesheets().add("/test/resources/css/general.css");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(owner);
        dialog.setResizable(false);
        dialog.getIcons().add(new Image("/test/resources/images/icon.png"));
        this.label.setText(label);
    }


    public void showDialog() {
        root.getChildren().add(mainPane);
        vbox.setSpacing(20);
        vbox.setAlignment(Pos.CENTER);
        vbox.setMinSize(330, 120);
        vbox.setPrefWidth(label.getWidth());
        vbox.getChildren().addAll(label,progressIndicator);
        mainPane.setTop(vbox);
        dialog.setScene(scene);
        dialog.show();
    }


    @Override
    public void call() {
        dialog.close();
    }
}