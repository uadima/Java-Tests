package test.myCustomUtils;

import okhttp3.*;
import test.models.Test;

import java.io.*;
import java.util.Objects;

import static test.Start.preferences;
import static test.Start.test;
import static test.models.Test.test_result;


public class NetworkManager {
    private final static String SERVER_URL = "http://uadima.ml/tests/api.php";
    public final static String RESULTS_URL = "http://uadima.ml/tests/";
    public String username;
    public String responseString;
    public boolean result;
    private OkHttpClient client = new OkHttpClient();
    public Headers headers; //наши куки файлы
    public boolean rememberMe=true;

    public NetworkManager() {
    }


    public void auth() {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("auth", "someValue")
                .addFormDataPart("username", username)
                .build();
        Request request = new Request.Builder()
                .url(SERVER_URL)
                .post(body)
                .build();
        Response response;

        try {
            response = client.newCall(request).execute();
            headers = response.headers();
            if (rememberMe) {
                System.out.println("Remember me");
                for (String header : response.headers("Set-Cookie")) {
                    System.out.println("Putting " + header + " For user "+username);
                    preferences.put("KEY_SESSION", header);
                    preferences.put("KEY_LOGIN", username);
                }
            }
            else {
                System.out.println("Don`t remember me");
            }
            responseString = response.body().string();
            result = responseString.equals("Session started for user " + username + ".");
        } catch (IOException e) {
            //    e.printStackTrace();
            result = false;
        }
    }


    public void deauth() {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("deauth", "someValue")
                .build();
        Request request = null;
        try {
            request = new Request.Builder()
                    .addHeader("Cookie", headers.get("Set-Cookie"))
                    .url(SERVER_URL)
                    .post(body)
                    .build();
        } catch (Exception e) {
            responseString = "Failed to deauth,probably headers not set";
            result=false;
        }
        Response response;
        try {
            response = client.newCall(request).execute();
            responseString = response.body().string();
            preferences.remove("KEY_SESSION");
            preferences.remove("KEY_LOGIN");
            result = responseString.equals("Session destroyed.");
        } catch (IOException e) {
            responseString = "Failed to deauth";
            result = false;
        }
    }

    public String getTests() {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("get_tests", "")
                .build();
        Request request = null;
        try {
            request = new Request.Builder()
                    .addHeader("Cookie", headers.get("Set-Cookie"))
                    .url(SERVER_URL)
                    .post(body)
                    .build();
        } catch (Exception e) {
            responseString = "Failed to get tests,probably headers not set";
            result=false;
        }
        Response response;
        try {
            response = client.newCall(request).execute();
            responseString = response.body().string();
            result=true;
        } catch (NullPointerException e) {
            System.out.println("cant get tests, headers are null");
            result=false;
        }catch (IOException e) {
            e.printStackTrace();
            responseString = "Failed to get tests";
            result=false;
        }
        return responseString;
    }

    public Test getTest(String name) {
        Test test = null;
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("get_test", "some value")
                .addFormDataPart("test_name", name)
                .build();
        Request request = new Request.Builder()
                .addHeader("Cookie", headers.get("Set-Cookie"))
                .url(SERVER_URL)
                .post(body)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            FileOutputStream fos = new FileOutputStream("./test.xml");
            fos.write(Objects.requireNonNull(response.body()).bytes());
            fos.close();
            test = XmlSerialization.fromXmlToObject("./test.xml");
            return test;
        } catch (IOException e) {
            e.printStackTrace();
            responseString = "Failed download test " + name + " " + e;
        }
        return null;
    }


    public void publishScore() {
        if ( (int) test_result>0) {

            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("publish_score", "someValue")
                    .addFormDataPart("username", username)
                    .addFormDataPart("test_name", test.getTest_name())
                    .addFormDataPart("score", Integer.toString((int) test_result))
                    .build();
            Request request = new Request.Builder()
                    .addHeader("Cookie", headers.get("Set-Cookie"))
                    .url(SERVER_URL)
                    .post(body)
                    .build();
            Response response;
            try {
                response = client.newCall(request).execute();
                responseString = response.body().string();
                result = responseString.equals("Result published");
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }
        }
        else{
            result = false;
        }
    }

    public void uploadTest(byte[] bytes, String test_name) {
/*        for (byte b : bytes){
            System.out.println((char) b);}*/

        File targetFile = new File("./"+test_name+".xml");
        OutputStream outStream = null;
        try {
            outStream = new FileOutputStream(targetFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            outStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody body = null;
        try {
            body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("upload_test", "someValue")
                    .addFormDataPart("test", test_name,
                            RequestBody.create(MediaType.parse("text/xml"), targetFile))
                    .build();
        } catch (NullPointerException e) {
            System.out.println("File not set");
            result=false;
            responseString="File not set";
        }
        Request request = new Request.Builder()
                    .addHeader("Cookie", headers.get("Set-Cookie"))
                    .url(SERVER_URL)
                    .post(body)
                    .build();
            Response response;
            try {
                response = client.newCall(request).execute();
                responseString = response.body().string();
                System.out.println(responseString);
                result = responseString.equals("File uploaded successfully");
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                responseString="Failed to upload test. Communication Error.";
            }
            finally {
                targetFile.delete();
            }
    }

    public void testConnection() {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("test_connection", "someValue")
                .build();
        Request request = null;
        try {
            request = new Request.Builder()
                    .addHeader("Cookie", headers.get("Set-Cookie"))
                    .url(SERVER_URL)
                    .post(body)
                    .build();
        } catch (NullPointerException e) {
            System.out.println("cant build request, headers are null");
        }
        Response response;
        try {
            response = client.newCall(request).execute();
            responseString = response.body().string();
            result = responseString.equals("You are logged in as " + username);
        } catch (NullPointerException e) {
            System.out.println("cant build request, headers are null");
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
    }
}



