package test.myCustomUtils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;

import java.io.IOException;
import java.net.URL;

import static test.Start.mainStage;

public class StageController {
    private static Image icon = new Image("/test/resources/images/icon.png");
    public static URL[] last = new URL[2];

    public StageController() {

    }

    public void next(URL resource) {
        goForward(resource);
        try {
            Parent root = FXMLLoader.load(resource);
            mainStage.getIcons().add(icon);
            Scene scene = new Scene(root, mainStage.getScene().getWidth(), mainStage.getScene().getHeight());
            scene.getStylesheets().add("/test/resources/css/general.css");
            mainStage.setScene(scene);
            mainStage.show();
        } catch (IOException e) {
            System.out.println("cant load scene");
            e.printStackTrace();
        }
    }

    public void previous() {
        try {
            Parent root = FXMLLoader.load(goBack());
            mainStage.getIcons().add(icon);
            Scene scene =  new Scene(root, mainStage.getScene().getWidth(), mainStage.getScene().getHeight());
            scene.getStylesheets().add("/test/resources/css/general.css");
            mainStage.minWidthProperty().bind(scene.widthProperty());
            mainStage.minHeightProperty().bind(scene.heightProperty());
            mainStage.setScene(scene);
            mainStage.show();
/*            mainStage.sizeToScene();*/
        } catch (IOException e) {
            System.out.println("cant load scene");
            e.printStackTrace();
        }

    }

    private void goForward(URL resource) {
        last[0] = last[1];
        last[1] = resource;
    }

    private URL goBack() {
        last[1] = last[0];
        return last[0];
    }

}
