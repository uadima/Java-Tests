package test.myCustomUtils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import static javafx.stage.StageStyle.UTILITY;

public class RetardAlert
{
    private String titleText="Retard alert!";
    private String contentText;
    private ImageView grapthics= new ImageView(this.getClass().getResource("/test/resources/images/retardalert.jpg").toString());
    private Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    Media media = new Media(this.getClass().getResource("/test/resources/sounds/uh-oh-retard-alert.mp3").toString());
    MediaPlayer mediaPlayer = new MediaPlayer(media);

    public Alert getDialog(String contentText, String confirmText) {
        grapthics.setFitHeight(120);
        grapthics.setFitWidth(200);
        alert.setGraphic(grapthics);
        mediaPlayer.play();
        this.alert.setContentText(contentText);
        this.alert.setHeaderText(null);
        this.alert.setTitle(titleText);
        ButtonType buttonType = new ButtonType(confirmText, ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().addAll(buttonType);
        alert.initStyle(UTILITY);
        alert.getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        alert.getDialogPane().getStylesheets().add(this.getClass().getResource("/test/resources/css/general.css").toString());
        return this.alert;
    }
    public Alert getDialog(String contentText) {
        return this.getDialog(contentText, "My bad...");
    }
}
