package test.myCustomUtils;

import javafx.animation.FadeTransition;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class ImageFadeEffects {
    ImageView imageView;
    FadeTransition ft = new FadeTransition();

    public ImageFadeEffects(ImageView imageView) {
        this.imageView = imageView;
    }

    public FadeTransition getAnimation() {
        ft.setNode(imageView);
        ft.setDuration(new Duration(3000));
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        return ft;
    }
}
