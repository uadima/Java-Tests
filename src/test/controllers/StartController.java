package test.controllers;

import com.jfoenix.controls.JFXToggleButton;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import test.myCustomUtils.RetardAlert;
import test.myCustomUtils.WorkIndicatorDialog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static test.Start.networkManager;
import static test.Start.stageController;

public class StartController {

    @FXML
    private HBox toogleButtonHbox;

    @FXML
    private Button startButton;

    @FXML
    private TextField userName;


    @FXML
    void keypressed(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
                startButton.fire();
                break;
        }
    }

    @FXML
    void initialize() {
        addToogleButton();
        initClickListener();
    }

    private void initClickListener() {
        initAuthClick();
    }

    private void initAuthClick() {
        startButton.setOnAction(event -> {
                    if (!isValid(userName.getText())) {
                        new RetardAlert().getDialog("Enter ur fucking name!", "Ok ok, i will!").showAndWait();
                        return;
                    } else {
                        networkManager.username = userName.getText();
                        WorkIndicatorDialog workIndicatorDialog = new WorkIndicatorDialog(startButton.getScene().getWindow(), "Communicating with a server...");
                        new Thread(getAuthRunnable(workIndicatorDialog)).start();
                        workIndicatorDialog.showDialog();
                    }
                }
        );
    }

    private boolean isValid(String str) {
        Pattern p = Pattern.compile("^[\\wа-яА-Я]{3,}+$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    private Runnable getAuthRunnable(WorkIndicatorDialog workIndicatorDialog) {
        return () -> {
            networkManager.auth();
            Platform.runLater(() -> authResult(workIndicatorDialog));
        };
    }

    private void authResult(WorkIndicatorDialog workIndicatorDialog) {
        workIndicatorDialog.call();
        if (networkManager.result) {
            stageController.next(this.getClass().getResource("/test/resources/view/tutorial.fxml"));
        } else {
            new RetardAlert().getDialog("Error during communication with a server", "Ok").showAndWait();
            System.out.println("Authentication failed");
        }
    }

    private void addToogleButton() {
        JFXToggleButton toggleButton = new JFXToggleButton();
        toggleButton.setText("Remember me");
        toggleButton.setId("ToogleButton");
        toggleButton.setSelected(true);
        toogleButtonHbox.getChildren().add(toggleButton);
        toggleButton.setOnAction(event -> {
            networkManager.rememberMe = toggleButton.isSelected();
        });
    }


}
