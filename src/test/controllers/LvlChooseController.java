package test.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import test.interfaces.Callback;
import test.models.CustomCellView;
import test.models.RefreshView;
import test.models.TestList;
import test.myCustomUtils.RetardAlert;
import test.myCustomUtils.WorkIndicatorDialog;

import static test.Start.*;

public class LvlChooseController {

    @FXML
    private ListView<TestList> testsListView;

    @FXML
    private Button uploadTestButton;

    @FXML
    private Button logoutButton;

    @FXML
    private Text logInfo;

    @FXML
    private AnchorPane anchorPaneRoot;


    @FXML
    void initialize() {
        logInfo.setText(networkManager.username);
        initListeners();
        WorkIndicatorDialog RetrievingTestsIndicatorDialog = new WorkIndicatorDialog(mainStage, "Retrieving test list...");
        new Thread(new Runnable() {
            TestList[] tests_array;
            @Override
            public void run() {
                Gson gson = new Gson();
                try {
                    tests_array = gson.fromJson(networkManager.getTests(), TestList[].class);
                } catch (JsonSyntaxException e) {
                    System.out.println("Problem during JSON Parsing");
                    e.printStackTrace();
                }
                Platform.runLater(() -> {
                    if (networkManager.result){
                    ((Callback) RetrievingTestsIndicatorDialog).call();
                    ObservableList<TestList> testList = FXCollections.observableArrayList(tests_array);
                    testsListView.setItems(testList);
                    testsListView.setStyle("-fx-control-inner-background: #202B33; -fx-background-insets: 0 ;");
                    testsListView.setCellFactory(new javafx.util.Callback<ListView<TestList>, ListCell<TestList>>() {
                        @Override
                        public ListCell<TestList> call(ListView<TestList> param) {
                            ListCell<TestList> cell = new ListCell<TestList>() {
                                @Override
                                protected void updateItem(TestList item, boolean empty) {
                                    super.updateItem(item, empty);
                                    if (item != null) {
                                        CustomCellView customCellView = new CustomCellView();
                                        customCellView.setInfo(item.test_name, item.author, item.description);
                                        setGraphic(customCellView.getBox());
                                    }

                                }
                            };
                            return cell;
                        }
                    });
                    }else {
                        ((Callback) RetrievingTestsIndicatorDialog).call();
                        new RetardAlert().getDialog("Cannot receive test list from server", "Ok").show();
                        anchorPaneRoot.getChildren().clear();
                        anchorPaneRoot.getChildren().add(new RefreshView().getBox());
                    }
                });

            }
        }).start();
        RetrievingTestsIndicatorDialog.showDialog();
    }

    private void initListeners(){
        WorkIndicatorDialog logOutIndicatorDialog = new WorkIndicatorDialog(mainStage, "Logging out...");
        logoutButton.setOnAction(event -> {
            new Thread(() -> {
                try {
                    networkManager.deauth();
                } catch (Exception e) {
                    System.out.println("Failed to deauth");
                }
                Platform.runLater(() -> {
                    ((Callback) logOutIndicatorDialog).call();
                    if (networkManager.result) {
                        stageController.next(this.getClass().getResource("/test/resources/view/start.fxml"));
                    } else {
                        new RetardAlert().getDialog("Error during logging out","Ok").show();
                    }
                });
            }).start();
            logOutIndicatorDialog.showDialog();
        });
        uploadTestButton.setOnAction(event -> {
            stageController.next(getClass().getResource("/test/resources/view/upload.fxml"));
        });
    }

}
