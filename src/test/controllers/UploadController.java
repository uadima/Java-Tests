package test.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import test.models.Test;
import test.myCustomUtils.WorkIndicatorDialog;
import test.myCustomUtils.XmlSerialization;
import zunayedhassan.AnimateFX.*;

import java.io.*;

import static test.Start.*;

public class UploadController {

    @FXML
    private ImageView statusImage;

    @FXML
    private VBox vBoxExample;

    @FXML
    private Text testDescription;

    @FXML
    private VBox vBoxResult;

    @FXML
    private Text xmlExample;

    @FXML
    private Text testAuthor;

    @FXML
    private Text testQuestionCount;

    @FXML
    private Text errorText;

    @FXML
    private Button backButton;

    @FXML
    private Button filePick;

    @FXML
    private Button uploadButton;

    @FXML
    private TextArea textArea;

    @FXML
    private Text testName;

    @FXML
    private VBox vBoxTestInfo;

    @FXML
    private Separator separator1;

    @FXML
    private Separator separator2;

    @FXML
    private Separator separator3;

    @FXML
    private Separator separator4;

    byte binaryTest[]=null;

    @FXML
    void initialize() {
        initListeners();
        bindWith();
        setVisible(vBoxExample);
        addStyle();
        uploadButton.setDisable(true);
        new TadaAnimation(filePick).Play();
    }
    private  Test newTest;
    private void bindWith() {
        textArea.setPrefHeight(mainStage.getHeight() * .8f);
        xmlExample.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.9f));
        testDescription.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        testAuthor.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        testName.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        testQuestionCount.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        separator1.maxWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        separator2.maxWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        separator3.maxWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
        separator4.maxWidthProperty().bind(mainStage.widthProperty().multiply(.7f));
    }

    private void initListeners() {

        backButton.setOnAction(event -> {
            stageController.previous();
        });

        uploadButton.setOnAction(event -> {
            errorText.setOpacity(0);
            WorkIndicatorDialog workIndicatorDialog = new WorkIndicatorDialog(mainStage, "Uploading test: " + testName.getText().replaceAll("\\s",""));
            asyncFileUpload(workIndicatorDialog);
            workIndicatorDialog.showDialog();
        });

        filePick.setOnAction(event -> {
            errorText.setOpacity(0);
            vBoxTestInfo.setOpacity(0);
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Test .XML File");
            FileChooser.ExtensionFilter textFilter = new FileChooser.ExtensionFilter("XML file(*.xml)", "*.xml");
            fileChooser.setSelectedExtensionFilter(textFilter);
            File fileInput = fileChooser.showOpenDialog(mainStage);
            if (fileInput != null) {
                System.out.println(fileInput.getAbsolutePath());
                if (checkFile(fileInput)) {
                    binaryTest=fileToByteArray(fileInput);
                    setVisible(vBoxResult);
                    uploadButton.setDisable(false);
                    statusImage.setVisible(true);
                    statusImage.setImage(new Image("/test/resources/images/right.png"));
                    /*     Animation */
                    BounceAnimation pulseAnimation = new BounceAnimation(uploadButton, 200);
                    pulseAnimation.GetTimeline().setDelay(Duration.millis(15));
                    FadeInAnimation fadeInAnimation = new FadeInAnimation(vBoxTestInfo);
                    fadeInAnimation.GetTimeline().setOnFinished(event1 -> {
                        pulseAnimation.Play();
                    });
                    fadeInAnimation.Play();
                    /*  ---------Animation end--------- */

                    vBoxTestInfo.setVisible(true);
                } else {
                    setVisible(vBoxExample);
                    xmlExample.setText("Wrong file type selected. Correct XML file structure showed below");
                    xmlExample.setFill(Color.RED);
                    /*     Animation */
                    new ShakeAnimation(xmlExample).Play();
                    /*  ---------Animation end--------- */
                    uploadButton.setDisable(true);
                }
            }
        });

    }

    private byte[] fileToByteArray(File fileInput) {
        byte[] bytes = null;
        FileInputStream fin = null;
        try {
           fin=new FileInputStream(fileInput);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            bytes = fin.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fin.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    private void asyncFileUpload(WorkIndicatorDialog workIndicatorDialog) {
        new Thread(() -> {
            networkManager.uploadTest(binaryTest,newTest.getTest_name());
            Platform.runLater(workIndicatorDialog::call);
            if (networkManager.result) {
                uploadButton.setDisable(true);
                statusImage.setImage(new Image("/test/resources/images/right.png"));

                /*     Animation */
                PulseAnimation pulseAnimation = new PulseAnimation(statusImage);
                errorText.setFill(Color.GREEN);
                BounceAnimation bounceAnimation = new BounceAnimation(backButton);
                bounceAnimation.GetTimeline().setDelay(Duration.millis(15));
                pulseAnimation.GetTimeline().setOnFinished(event1 -> bounceAnimation.Play());
                bounceAnimation.GetTimeline().setOnFinished(event1 -> {
                    new BounceInLeftAnimation(errorText, mainStage.getWidth() / 2).Play();
                    errorText.setVisible(true);
                });
                pulseAnimation.Play();

                /*  ---------Animation end--------- */
            } else {
                errorText.setFill(Color.RED);
                statusImage.setImage(new Image("/test/resources/images/wrong.png"));

                /*     Animation */
                ShakeAnimation shakeAnimation= new ShakeAnimation(statusImage);
                shakeAnimation.GetTimeline().setOnFinished(event1 -> {
                    new BounceInLeftAnimation(errorText, mainStage.getWidth() / 2).Play();
                    errorText.setVisible(true);
                });
                shakeAnimation.Play();
                /*  ---------Animation end--------- */
            }
            System.out.println(networkManager.responseString);
            errorText.setText("  Server response: " + networkManager.responseString);
        }).start();
    }

    private boolean checkFile(File file) {
        try {
            newTest = XmlSerialization.fromXmlToObject(file.getAbsolutePath());
            testName.setText("  " + newTest.getTest_name());
            testAuthor.setText("  @" + newTest.getAuthor());
            testDescription.setText("  Description: " + newTest.getDescription());
            testQuestionCount.setText("  Questions count:  " + newTest.getQuestions().size());
        } catch (NullPointerException e) {
            System.out.println("Selected wrong type of file");
            return false;
        }
        return XmlSerialization.result;
    }

    private void setVisible(VBox vBox) {
        switch (vBox.getId()) {
            case "vBoxExample":
                vBoxExample.setVisible(true);
                vBoxExample.setManaged(true);
                vBoxResult.setVisible(false);
                vBoxResult.setManaged(false);
                return;
            case "vBoxResult":
                vBoxExample.setVisible(false);
                vBoxExample.setManaged(false);
                vBoxResult.setVisible(true);
                vBoxResult.setManaged(true);
                return;
        }
    }
    private void addStyle(){
        vBoxTestInfo.getStyleClass().add("bordered-titled-border");
        testName.getStyleClass().add("separator");
        testQuestionCount.getStyleClass().add("separator");
        testAuthor.getStyleClass().add("separator");
        testDescription.getStyleClass().add("separator");
    }
}
