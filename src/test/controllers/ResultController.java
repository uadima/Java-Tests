package test.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import test.models.Test;
import test.myCustomUtils.NetworkManager;
import zunayedhassan.AnimateFX.PulseAnimation;

import static test.Start.*;
import static test.models.Test.test_result;

public class ResultController {

    @FXML
    private Text link;

    @FXML
    private Text logInfo;


    @FXML
    private Text congratulation;

    @FXML
    private Button uploadTestButton;

    @FXML
    private Text resultPublishing;

    @FXML
    private Button backButton;

    @FXML
    private Text mark;


    static int temp=1;

    @FXML
    void initialize() {
        initListeners();
        logInfo.setText(networkManager.username);
        if (networkManager.result) {
            congratulation.setText("Congratulations " + networkManager.username + ". You`ve just completed the test '" + test.getTest_name() + "' and u gave right answers on");
            animateResults();
        }else if(Test.test_result == 0 ){
            congratulation.setText("Shame on u " + networkManager.username + ". You`ve just failed the test '" + test.getTest_name() + "' and u gave right answers on");
            mark.setText(Integer.toString((int) test_result) + "%" + " questions");
            mark.setFill(Color.RED);
            resultPublishing.setText("Ur results would not be uploaded to the laderboard. But anyway u a welcome to see results of others at ");
        }else {
            congratulation.setText("Congratulations " + networkManager.username + ". You`ve just completed the test '" + test.getTest_name() + "' and u gave right answers on");
            animateResults();
            resultPublishing.setFill(Color.RED);
            resultPublishing.setText(("We are sorry, smth goes wrong and ur results were not uploaded to the laderboard."));
        }


    }

    private void initListeners() {
        link.setOnMouseClicked(event -> {
            startContext.openURL(NetworkManager.RESULTS_URL);

        });
        backButton.setOnAction(event -> {
            stageController.next(getClass().getResource("/test/resources/view/lvlChoose.fxml"));
        });
        uploadTestButton.setOnAction(event -> {
            stageController.next(getClass().getResource("/test/resources/view/upload.fxml"));
        });
    }

    private void animateResults(){
        temp=1;
        double sleepTime=2/test_result;
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(sleepTime), event -> {
           mark.setText(temp + "%" + " questions");
           temp++;
        }));
        timeline.setCycleCount((int)test_result);
        timeline.setOnFinished(event -> {
            new PulseAnimation(mark).Play();
        });
        timeline.play();

    }
}
