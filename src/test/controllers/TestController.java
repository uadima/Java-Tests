package test.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import test.interfaces.Callback;
import test.models.CustomGrid;
import test.models.Question;
import test.myCustomUtils.WorkIndicatorDialog;
import zunayedhassan.AnimateFX.BounceInLeftAnimation;
import zunayedhassan.AnimateFX.PulseAnimation;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static test.Start.*;
import static test.models.Test.published;
import static test.models.Test.test_result;

public class TestController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private AnchorPane rootAnchor;

    @FXML
    private Button finishTestButton;

    @FXML
    private Text question;

    @FXML
    private HBox hbox_1;

    @FXML
    private HBox hbox_3;

    @FXML
    private HBox hbox_2;

    @FXML
    private HBox hbox_4;


    @FXML
    private Text logInfo;

    @FXML
    private Button nextButton;

    @FXML
    private Text answer2;

    @FXML
    private Text answer1;

    @FXML
    private RadioButton radio3;

    @FXML
    private Text answer4;

    @FXML
    private Text answer3;

    @FXML
    private RadioButton radio4;

    @FXML
    private RadioButton radio1;

    @FXML
    private RadioButton radio2;

    @FXML
    private Text time;

    @FXML
    private GridPane controlGrid;

    @FXML
    private Text testName;

    @FXML
    private Button prevButton;

    @FXML
    private Text currentQuestion;

    @FXML
    private CheckBox checkBox1;

    @FXML
    private CheckBox checkBox2;

    @FXML
    private CheckBox checkBox3;

    @FXML
    private CheckBox checkBox4;


    private Integer current = 0;
    private int gridSizeW;
    private int gridSizeH;
    private boolean answers[][]=new boolean[test.getQuestions().size()][5];
    private int timeLeft;
    private Timeline timeline;
    private List<RadioButton> radios = new ArrayList<RadioButton>();
    private List<CheckBox> checkBoxes = new ArrayList<CheckBox>();


    @FXML
    void initialize() {
        logInfo.setText(networkManager.username);
        testName.setText(test.getTest_name());
        timeLeft=test.getQuestions().size()*60;
        time.setText(""+(timeLeft/60)+"m. "+(timeLeft%60)+"s.");
        prepareElements();
        prepareGrid();
        prepareQuestion();
        setListeners();
        bindWith();
        setTimer();
    }

    private void prepareElements(){
        radios.add(radio1);
        radios.add(radio2);
        radios.add(radio3);
        radios.add(radio4);
        checkBoxes.add(checkBox1);
        checkBoxes.add(checkBox2);
        checkBoxes.add(checkBox3);
        checkBoxes.add(checkBox4);
    }

    public void next() {
        isCompleted();
        System.out.println("current question is " + (current + 1));
        if (current == test.getQuestions().size() - 1) {
            current = 0;
        } else {
            ++current;
        }
        prepareQuestion();
        updateIndex();
        prepareGrid();
        System.out.println("next question is " + (current + 1));
    }

    public void previous() {
        isCompleted();
        System.out.println("current question is " + (current + 1));
        if (current == 0) {
            current = test.getQuestions().size() - 1;
            System.out.println("previous question is " + (test.getQuestions().size()));
        } else {
            --current;
            System.out.println("previous question is " + (current));
        }
        prepareQuestion();
        updateIndex();
        prepareGrid();
    }

    public void next(int n) {
        isCompleted();
        prepareGrid();
        System.out.println("current question is " + (current + 1));
        current = n;
        prepareQuestion();
        updateIndex();
        prepareGrid();
        System.out.println("next question is " + (n + 1));
    }

    /*     Prepare question list   */
    public void prepareQuestion() {
        chooseBox();
        question.setOpacity(0);
        answer1.setOpacity(0);
        answer2.setOpacity(0);
        answer3.setOpacity(0);
        answer4.setOpacity(0);
        radio1.setOpacity(0);
        radio2.setOpacity(0);
        radio3.setOpacity(0);
        radio4.setOpacity(0);
        checkBox1.setOpacity(0);
        checkBox2.setOpacity(0);
        checkBox3.setOpacity(0);
        checkBox4.setOpacity(0);
        question.setText(test.getQuestions().get(current).getQuestion());
        answer1.setText(test.getQuestions().get(current).getAnswer1());
        answer2.setText(test.getQuestions().get(current).getAnswer2());
        answer3.setText(test.getQuestions().get(current).getAnswer3());
        answer4.setText(test.getQuestions().get(current).getAnswer4());
        if (chooseBox()=="Check box"){
            checkBox1.setSelected(answers[current][1]);
            checkBox2.setSelected(answers[current][2]);
            checkBox3.setSelected(answers[current][3]);
            checkBox4.setSelected(answers[current][4]);
        }else {
            radio1.setSelected(answers[current][1]);
            radio2.setSelected(answers[current][2]);
            radio3.setSelected(answers[current][3]);
            radio4.setSelected(answers[current][4]);
        }
        new BounceInLeftAnimation(question, mainStage.getMinWidth()/8).Play();
        new BounceInLeftAnimation(radio1, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(radio2, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(radio3, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(radio4, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(checkBox1, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(checkBox2, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(checkBox3, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(checkBox4, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(answer1, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(answer2, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(answer3, mainStage.getMinWidth()/4).Play();
        new BounceInLeftAnimation(answer4, mainStage.getMinWidth()/4).Play();
    }

    private void prepareGrid() {
        controlGrid.setGridLinesVisible(false);
        gridSizeW = (int) Math.sqrt(test.getQuestions().size()) + 1;
        gridSizeH = calcHeight();
        final int scaleW = 37;
        final int scaleH = 30;
        controlGrid.getRowConstraints().clear();
        controlGrid.getColumnConstraints().clear();
        controlGrid.setPrefHeight(gridSizeW * scaleH);
        controlGrid.setMaxHeight(gridSizeW * scaleH);
        controlGrid.setMinHeight(gridSizeW * scaleH);
        controlGrid.setPrefWidth(gridSizeW * scaleW);
        controlGrid.setMaxWidth(gridSizeW * scaleW);
        controlGrid.setMinWidth(gridSizeW * scaleW);

        /*        Build our grid  corresponding to questions array size*/
        int counter = 0;
        outer:
        for (int i = 0; i < gridSizeW; i++) {
            for (int j = 0; j < gridSizeW && counter < test.getQuestions().size(); j++) {
                RowConstraints rConstraint = new RowConstraints();
                rConstraint.setPercentHeight(100 / gridSizeH);
                controlGrid.getRowConstraints().add(rConstraint);
                counter++;
                if (counter == gridSizeW - 1) {
                    break outer;
                }
            }
            ColumnConstraints cConstraint = new ColumnConstraints();
            cConstraint.setPercentWidth(100 / gridSizeW);
            controlGrid.getColumnConstraints().add(cConstraint);
        }

        /*      Add buttons to the grid  */
        counter = 0;
        for (int i = 0; i < gridSizeW && counter < test.getQuestions().size(); i++)
            for (int j = 0; j < gridSizeW && counter < test.getQuestions().size(); j++) {
                CustomGrid customGrid = new CustomGrid(this, counter);
                customGrid.setText(Integer.toString(counter + 1));
                controlGrid.add(customGrid.getButton_grid(), j, i);
                counter++;
            }
    }

    public void finishTest() {
        timeline.stop();
        next();
        test_result = 0;
        int counter = 0;
        for (Question q : test.getQuestions()) {
            double penalty = 0;
            double right = 0;
            for (int i : q.getRightanswer()) {
                if (answers[counter][i]) {
                    right = right + (double) 1 / q.getRightanswer().length;
                    //  test_result=test_result+((double) 1/q.getRightanswer().length);
                }
                System.out.println("Правильный ответ на " + (counter + 1) + " вопрос был " + i + " вариант, а Вы ответили на него " + answers[counter][i]);

            }
            for (int i = 1; i < 5; i++) {
                if (answers[counter][i] && !ifContain(i, q.getRightanswer())) {
                    if (right != 0 && right > penalty) {
                        penalty = penalty + (double) 1 / q.getRightanswer().length;
                        System.out.println("Депримация за не верный ответ " + answers[counter][i] + " на вопрос №" + (counter + 1) + " Вариант " + i);
                    }
                }
            }
            test_result = test_result + right - penalty;
            counter++;
        }
        test_result = test_result / ((double) test.getQuestions().size()) * 100;
        System.out.println("Вы ответили правильно на " + test_result + "% вопросов из теста '" + test.getTest_name() + "'");
    }

    // Simple check if array of int contain some number

    public boolean ifContain(int x, int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            if (x == ar[i]) {
                return true;
            }
        }
        return false;

    }

    public void setListeners() {
        prevButton.setOnAction(event -> previous());
        nextButton.setOnAction(event -> next());
        finishTestButton.setOnAction(event -> {
            finishTest();
            WorkIndicatorDialog workIndicatorDialog = new WorkIndicatorDialog(mainStage, "Publishing results");
            new Thread(new Runnable() {
                Callback callback = workIndicatorDialog;

                @Override
                public void run() {
                    networkManager.publishScore();
                    System.out.println(networkManager.result ? "Results published" : "Results publishing failed");
                    published = networkManager.result;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            callback.call();
                            stageController.next(getClass().getResource("/test/resources/view/result.fxml"));
                        }
                    });
                }
            }).start();
            workIndicatorDialog.showDialog();
        });
        for (RadioButton radio:radios
             ) {
            radio.setOnMouseClicked(event -> {
                int radio_index= Integer.parseInt(radio.getId().substring(radio.getId().lastIndexOf('o')+1));
                unselectOthers(radio_index);
            });
        }
        hbox_1.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (chooseBox()=="Check box"){
                checkBox1.setSelected(!checkBox1.isSelected());}
                else {
                    unselectOthers(1);
                }
            }
        });
        hbox_2.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (chooseBox()=="Check box"){
                        checkBox2.setSelected(!checkBox2.isSelected());}
                    else {
                        unselectOthers(2);
                    }
                }
            }
        });
        hbox_3.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (chooseBox()=="Check box"){
                        checkBox3.setSelected(!checkBox3.isSelected());}
                    else {
                        unselectOthers(3);
                    }
                }
            }
        });
        hbox_4.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (chooseBox()=="Check box"){
                        checkBox4.setSelected(!checkBox4.isSelected());}
                    else {
                        unselectOthers(4);
                    }
                }
            }
        });

        rootAnchor.setOnKeyPressed(event -> {
            switch (event.getCode()){
                case ENTER:
                case RIGHT:
                    next();
                    break;
                case LEFT:
                    previous();
                    break;
            }
        });
    }

    public int calcHeight() {
        int temp = test.getQuestions().size();
        int h = 0;
        do {
            temp = temp - gridSizeW;
            h++;
        } while (temp > gridSizeW);
        return h;
    }

    private void isCompleted(){
        if (chooseBox()=="Check box") {
            if (checkBox1.isSelected() || checkBox2.isSelected() || checkBox3.isSelected() || checkBox4.isSelected()){
                test.getQuestions().get(current).setCompleted(true);
            }else {
                test.getQuestions().get(current).setCompleted(false);
            }
            answers[current][1] = checkBox1.isSelected();
            answers[current][2] = checkBox2.isSelected();
            answers[current][3] = checkBox3.isSelected();
            answers[current][4] = checkBox4.isSelected();
        }
        else {
            if (radio1.isSelected() || radio2.isSelected() || radio3.isSelected() || radio4.isSelected()){
                test.getQuestions().get(current).setCompleted(true);
            }else {
                test.getQuestions().get(current).setCompleted(false);
            }
            answers[current][1] = radio1.isSelected();
            answers[current][2] = radio2.isSelected();
            answers[current][3] = radio3.isSelected();
            answers[current][4] = radio4.isSelected();
        }
    }
    private void setTimer(){
        timeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
            updateIndex();
                timeLeft--;
                time.setText(""+(timeLeft/60)+"m. "+(timeLeft%60)+"s.");
                if (timeLeft<5){
                    new PulseAnimation(time).Play();
                }
        }));
        timeline.setCycleCount(test.getQuestions().size()*60);
        timeline.setOnFinished(event -> {
            System.out.println("Time has gone");
            finishTestButton.fire();
        });
        timeline.play();
    }
    private void updateIndex(){
        currentQuestion.setText("№"+String.valueOf(current+1));
    }

    private  void bindWith(){
/*        question.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.4f));*/
        answer1.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
        answer2.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
        answer3.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
        answer4.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.5f));
    }

    public javafx.scene.paint.Color colorChoose(int n){
        javafx.scene.paint.Color color = Color.web("#16A18F");
        if (test.getQuestions().get(n).isCompleted()){
            color= Color.ORANGE;
        }
        if (n==current){
            color= Color.web("#3C3C4F",0.7);
        }
        return color;
    }

    private String chooseBox(){
        if(test.getQuestions().get(current).getRightanswer().length>1){
            for (RadioButton radio:radios
                 ) {
                radio.setDisable(true);
                radio.setVisible(false);
            }
            for (CheckBox checkbox:checkBoxes
                 ) {
                checkbox.setDisable(false);
                checkbox.setVisible(true);
            }
            return "Check box";
        }
        else {
            for (RadioButton radio:radios
                    ) {
                radio.setDisable(false);
                radio.setVisible(true);
            }
            for (CheckBox checkbox:checkBoxes
                    ) {
                checkbox.setDisable(true);
                checkbox.setVisible(false);
            }
            return "radio button";
        }
    }

    private void unselectOthers(int n){
        for (int i = 1; i < 5; i++) {
            if (i!=n){
             radios.get(i-1).setSelected(false);
            }else {
                radios.get(i-1).setSelected(true);
            }
        }
    }

}
