package test.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import test.myCustomUtils.ImageFadeEffects;
import test.myCustomUtils.TypingMachine;

import java.net.URL;
import java.util.ResourceBundle;

import static test.Start.mainStage;
import static test.Start.stageController;


public class TutorialController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox vBoxBottom;

    @FXML
    private Button backButton;

    @FXML
    private Button acceptButton;

    @FXML
    private Text tutorialStoryOne;

    @FXML
    private Text tutorialStoryTwo;

    @FXML
    private ImageView imageTop;

    @FXML
    private Text tutorialStoryFour;


    @FXML
    private Text tutorialStoryThree;

    @FXML
    private ImageView imageBottom;

    @FXML
    void initialize() {
        acceptButton.getParent().setVisible(false);
        imageTop.setOpacity(0);
        imageBottom.setOpacity(0);
        bindWith();
        initListeners();
        new ImageFadeEffects(imageTop).getAnimation().play();
        ImageFadeEffects imageBottomFadeIn = new ImageFadeEffects(imageBottom);
        TypingMachine typingMachineTutorialStoryOne = new TypingMachine(tutorialStoryOne);
        TypingMachine typingMachineTutorialStoryTwo = new TypingMachine(tutorialStoryTwo);
        TypingMachine typingMachineTutorialStoryThree = new TypingMachine(tutorialStoryThree);
        TypingMachine typingMachineTutorialStoryFour = new TypingMachine(tutorialStoryFour);
        typingMachineTutorialStoryOne.setStory("So u wanna be a java developer? To earn so much as u can only carry out? To write code like a god? Well sounds great... But what about ur actual knowledge? Let`s chek it out! ");
        typingMachineTutorialStoryTwo.setStory("First u need to know boy, that good things doesn`t happen by themselves. So for the first time i`l advice to pass some test!");
        typingMachineTutorialStoryThree.setStory("First u need to know boy, that good things doesn`t happen by themselves. So for the first time i`l advice to pass some test! Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla ");
        typingMachineTutorialStoryFour.setStory("First u need to know boy, that good things doesn`t happen by themselves. So for the first time i`l advice to pass some test! Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla Bla bla ");
        typingMachineTutorialStoryOne.getAnimation().play();
        typingMachineTutorialStoryOne.getAnimation().setOnFinished(event -> typingMachineTutorialStoryTwo.getAnimation().play());
        typingMachineTutorialStoryTwo.getAnimation().setOnFinished((ActionEvent event) -> {
            imageBottomFadeIn.getAnimation().play();
            typingMachineTutorialStoryThree.getAnimation().play();
        });
        typingMachineTutorialStoryThree.getAnimation().setOnFinished(event -> typingMachineTutorialStoryFour.getAnimation().play());
        typingMachineTutorialStoryFour.getAnimation().setOnFinished(event -> acceptButton.getParent().setVisible(true));



    }
    private  void bindWith(){
        tutorialStoryOne.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.6f));
        tutorialStoryTwo.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.6f));
        vBoxBottom.setMinWidth(mainStage.getWidth()*.6f);
        tutorialStoryThree.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.6f));
        tutorialStoryFour.wrappingWidthProperty().bind(mainStage.widthProperty().multiply(.6f));
    }

    private void initListeners(){
        backButton.setOnAction(event -> {
            stageController.previous();
            //new RetardAlert().getDialog("There is NO WAY OUT, boy!").showAndWait();
        });
        acceptButton.setOnAction(event -> {
            //    StageController.next(acceptButton, getClass().getResource("/test/resources/view/lvlChoose.fxml"));
            stageController.next(getClass().getResource("/test/resources/view/lvlChoose.fxml"));
        });
    }
}
