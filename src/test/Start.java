package test;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import okhttp3.Headers;
import test.interfaces.Callback;
import test.models.Test;
import test.myCustomUtils.NetworkManager;
import test.myCustomUtils.StageController;
import test.myCustomUtils.WorkIndicatorDialog;

import java.io.IOException;
import java.util.prefs.Preferences;

import static test.myCustomUtils.StageController.last;

public class Start extends Application {

    public static NetworkManager networkManager = new NetworkManager();
    public static Stage mainStage;
    public static StageController stageController = new StageController();
    public static Start startContext;
    public static Test test = new Test();
    public static Preferences preferences;
    public static final int SCREEN_WIDTH=1200;
    public static final int SCREEN_HEIGHT=750;

    @Override
    public void init() throws Exception {
        System.out.println("init");
        loadFonts();
        super.init();
    }

    @Override
    public void stop() throws Exception {
        if(!networkManager.rememberMe){
            preferences.remove("KEY_SESSION");
            preferences.remove("KEY_LOGIN");
        }
        super.stop();
    }

    @Override

    public void start(Stage primaryStage) {
        startContext = this;
        mainStage = primaryStage;
        WorkIndicatorDialog workIndicatorDialog = new WorkIndicatorDialog(mainStage,"Restoring session...");
        new Thread(new Runnable() {
            Callback callback = workIndicatorDialog;
            boolean session_exists;
            @Override
            public void run() {
                session_exists=sessionCheck();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if(session_exists){
                            callback.call();
                            loadLvlChooseScene();
                        }
                        else {
                            callback.call();
                            loadStartScene();
                        }

                    }
                });
            }
        }).start();
        workIndicatorDialog.showDialog();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public void openURL(String url) {
        getHostServices().showDocument(url);
    }

    private void loadFonts() {
/*---------      Well, this code works well until artifact build, in compiled jar it return Null pointer ----------*/
/*        final File folder = new File(getClass().getResource("/test/resources/fonts/").getPath());
        try {
            for (final File fileEntry : folder.listFiles()) {
                if (!fileEntry.isDirectory()) {
                    String name = fileEntry.getName();
                    Font.loadFont(
                            getClass().getResource("/test/resources/fonts/"+name).toExternalForm(),
                            10
                    );
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }*/
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/Chewy-Regular.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/Kalam-Light.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/Kalam-Regular.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/LobsterTwo-Regular.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/Philosopher-Bold.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/Philosopher-Regular.ttf").toExternalForm(),
                10
        );
        Font.loadFont(
                getClass().getResource("/test/resources/fonts/ZillaSlab-MediumItalic.ttf").toExternalForm(),
                10
        );
    }
    private boolean sessionCheck(){
        preferences = Preferences.userRoot().node("java-tests");
        System.out.println(preferences.get("KEY_SESSION",""));
        System.out.println(preferences.get("KEY_LOGIN",""));
        if(preferences.get("KEY_SESSION","")!="" && preferences.get("KEY_LOGIN","")!=""){
            networkManager.username = preferences.get("KEY_LOGIN","");
            Headers headers = new Headers.Builder().add("Set-Cookie",preferences.get("KEY_SESSION","")).build();
            networkManager.headers= headers;
            networkManager.testConnection();
            return networkManager.result;
        }
        return false;
    }

    private void loadStartScene(){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/test/resources/view/start.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStage.setTitle("Java Test");
        mainStage.minWidthProperty().setValue(SCREEN_WIDTH);
        mainStage.minHeightProperty().setValue(SCREEN_HEIGHT);
        Scene scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
        scene.getStylesheets().add("/test/resources/css/general.css");
        mainStage.setScene(scene);
        mainStage.getIcons().add(new Image("/test/resources/images/icon.png"));
        mainStage.show();
        last[1] = getClass().getResource("/test/resources/view/start.fxml");
    }
    private void loadLvlChooseScene(){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/test/resources/view/lvlChoose.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStage.setTitle("Java Test");
        mainStage.minWidthProperty().setValue(SCREEN_WIDTH);
        mainStage.minHeightProperty().setValue(SCREEN_HEIGHT);
        Scene scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
        scene.getStylesheets().add("/test/resources/css/general.css");
        mainStage.setScene(scene);
        mainStage.getIcons().add(new Image("/test/resources/images/icon.png"));
        mainStage.show();
        last[1] = getClass().getResource("/test/resources/view/lvlChoose.fxml");
    }
}

