package test.interfaces;

public interface Callback {
    void call();
}
